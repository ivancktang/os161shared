#include <types.h>
#include <kern/errno.h>
#include <limits.h>
#include <lib.h>
#include <coremap.h>
#include <spinlock.h>

struct ppage *coremap;

//spinlock for coremap
struct spinlock coremap_lock;

//global var for first paddr
paddr_t firstpaddr;

unsigned coremap_length;

void
coremap_init(void)
{
	paddr_t lastpaddr;
	lastpaddr = ram_getsize();
	firstpaddr = ram_getfirstfree();

	unsigned mem_available = lastpaddr - firstpaddr;

	//calculating max index for coremap
	coremap_length = (mem_available + PAGE_SIZE -1)/PAGE_SIZE;

	//placing coremap to first physical address
	coremap = (struct ppage *) PADDR_TO_KVADDR(firstpaddr);

	for (unsigned i=0; i<coremap_length; i++) {
		coremap[i].allocated = 0;
	}

	//allocate first entry of coremap for coremap
	coremap[0].allocated = 1;

	spinlock_init(&coremap_lock);
}

//finds next free physical page and return paddr 
paddr_t
coremap_addentry(unsigned long npages)
{
	unsigned i;
	unsigned pagesAllocated = 0;

	spinlock_acquire(&coremap_lock);

	//update coremap with allocating npages
	for (i = 0; i < coremap_length; i++) {
		if (pagesAllocated == npages) break;
		if (coremap[i].allocated == 0) {
			coremap[i].allocated = 1;
			coremap[i].frame = PADDR_TO_KVADDR(firstpaddr + (i * PAGE_SIZE) );
			pagesAllocated++;
		}
	}

	spinlock_release(&coremap_lock);

	return firstpaddr + (paddr_t)((i-npages)*PAGE_SIZE);
}

void
coremap_evict(vaddr_t addr)
{
	//evict sequentially
	spinlock_acquire(&coremap_lock);	

	for (unsigned i = 1; i < coremap_length; i++) {
		if (coremap[i].frame == addr) {
			if (coremap[i].allocated == 0) {
				kprintf("There is free space, we dont need to evict\n");
				break;
			} else {
				coremap[i].allocated = 0;
				coremap[i].frame = 0;
				break;
			}
		}
	}

	spinlock_release(&coremap_lock);
}
