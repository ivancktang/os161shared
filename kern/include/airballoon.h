#ifndef _AIRBALLOON_H_
#define _AIRBALLOON_H_

/*
 *Header file for Airballoon problem
 */

#include <synch.h>
#include <thread.h>
#include <lib.h>

#define NROPES 16

static int ropes_left = NROPES;

// Data structures for rope mappings

struct rope{
	volatile unsigned hook_num;
	volatile unsigned stake_num;
	struct lock *rope_lock;
};

struct rope rope_list[NROPES];

void init_rope_list(void);
unsigned get_rand_stake(struct rope *rope_list);
unsigned get_rand_hook(struct rope *rope_list);

// Synchronization primitives 
//static struct lock *rope_lock;

/*
 * Describe your design and any invariants or locking protocols 
 * that must be maintained. Explain the exit conditions. How
 * do all threads know when they are done?  
 *
 * Explained in comments of the code
 */

static struct lock *rope_locks[NROPES];
struct lock *balloon_lock;
struct lock *balloon_lock2;
struct lock *main_thread_lock;

#endif
