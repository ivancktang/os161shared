#ifndef _COREMAP_H_
#define _COREMAP_H_

#include <addrspace.h>

struct __attribute__((__packed__)) ppage {
	unsigned allocated:1;
	
	//physical page frame
	vaddr_t frame:20;
};

//make coremap array global
extern struct ppage *coremap;
void coremap_init(void);
paddr_t coremap_addentry(unsigned long npages);
void coremap_evict(vaddr_t addr);

#endif /*_COREMAP_H_*/
