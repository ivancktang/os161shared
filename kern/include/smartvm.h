#ifndef _SMARTVM_H_
#define _SMARTVM_H_

#include <types.h>

paddr_t getppages(unsigned long npages);
vaddr_t alloc_kpages(unsigned npages);
void free_kpages(vaddr_t addr);
void vm_tlbshootdown_all(void);
void vm_tlbshootdown(const struct tlbshootdown *ts);
int vm_fault(int faulttype, vaddr_t faultaddress);
void vm_bootstrap(void);

#endif
