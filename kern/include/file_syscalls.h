#ifndef _FILE_SYSCALLS_H_
#define _FILE_SYSCALLS_H_

int sys_open(userptr_t filename, int flags, int *retval);
int sys_close(int fd, int *retval);
int sys_write(int fd, void *buf, size_t nbytes, int *retval);
int sys_read(int fd, void *buf, size_t buflen, int *retval);
int sys_lseek(int fd, uint64_t pos, int whence, off_t *retval);
int sys_dup2(int old_fd, int new_fd, int *retval);
int sys_chdir(userptr_t pathname, int *retval);
int sys___getcwd(userptr_t buf, size_t buflen, int *retval);

#endif
