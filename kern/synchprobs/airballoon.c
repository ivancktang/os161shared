/*
 * Driver code for airballoon problem
 */
#include <types.h>
#include <lib.h>
#include <thread.h>
#include <test.h>
#include <synch.h>
#include <airballoon.h>

/*
 *Initializes hooks with stakes
 *Maps stake 0 with hook 0, stake 1 with hook 1...
*/

void
init_rope_list(void)
{
	for (int n = 0; n < NROPES; n++){
		rope_list[n].hook_num = n;
		rope_list[n].stake_num = n;
		rope_locks[n] = lock_create("rope_lock");
	}
}

/*
 *pass in rope_list
 *will return a rope which is attached
 *NROPES+1 means the rope is not attached
 */
unsigned
get_rand_stake(struct rope *rope_list)
{
	unsigned n = 0;
	n = random()%NROPES;
	while(rope_list[n].hook_num == NROPES+1 || rope_list[n].stake_num == NROPES+1){			//while ropes are out of bound (cut) find another index
		n = random()%NROPES;
	}
	return n;
}

/*
 *pass in rope_list
 *will return a rope which is attached
 */
unsigned
get_rand_hook(struct rope *rope_list)
{
	unsigned n = 0;
	n = random()%NROPES;
	while(rope_list[n].hook_num == NROPES+1 || rope_list[n].stake_num == NROPES+1){
		n = random()%NROPES;
	}
	return n;
}

static
void
dandelion(void *p, unsigned long arg)
{
	(void)p;
	(void)arg;
	
	unsigned index = 0;
	kprintf("Dandelion thread starting\n");
	
	while(ropes_left != 0){
		lock_acquire(rope_locks[index]);

		if(rope_list[index].hook_num == NROPES+1 || rope_list[index].stake_num == NROPES+1){	//if rope is out of bound (cut) find another index
			index = get_rand_hook(rope_list);
		}
		ropes_left--;
		kprintf("Dandelion severed rope %d\n", rope_list[index].hook_num);
		rope_list[index].hook_num = NROPES+1;						//having hook set out of bounds means it is cut
		rope_list[index].stake_num = NROPES+1;						//having rope set out of bounds means it is cut

		lock_release(rope_locks[index]);
	}

	lock_release(balloon_lock);								//releases the first lock which is keeping balloon asleep

	kprintf("Dandelion thread done\n");
}

static
void
marigold(void *p, unsigned long arg)
{
	(void)p;
	(void)arg;

	unsigned index = 1;
	kprintf("Marigold thread starting\n");

	while(ropes_left != 0){
		lock_acquire(rope_locks[index]);

		if(rope_list[index].hook_num == NROPES+1 || rope_list[index].stake_num == NROPES+1){
			index = get_rand_hook(rope_list);
		}
		ropes_left--;
		kprintf("Marigold severed rope %d from stake %d\n", rope_list[index].hook_num, rope_list[index].stake_num);
		rope_list[index].hook_num = NROPES+1;
		rope_list[index].stake_num = NROPES+1;

		lock_release(rope_locks[index]);
	}

	lock_release(balloon_lock2);								//releases the second lock keeping balloon asleep

	kprintf("Marigold thread done\n");

}

static
void
flowerkiller(void *p, unsigned long arg)
{
	(void)p;
	(void)arg;
	
	kprintf("Lord FlowerKiller thread starting\n");

	unsigned index = 2;
	unsigned new_stake;
	while(ropes_left != 0){
		lock_acquire(rope_locks[index]);

		if(rope_list[index].hook_num == NROPES+1 || rope_list[index].stake_num == NROPES+1){
			index = get_rand_hook(rope_list);
		}
		new_stake = (rope_list[index].stake_num + random())%NROPES;			//finds a new stake number
		kprintf("Lord FlowerKiller switched rope %d from stake %d to stake %d\n", rope_list[index].hook_num, rope_list[index].stake_num, new_stake);
		rope_list[index].stake_num = new_stake;

		lock_release(rope_locks[index]);
	}
}

static
void
balloon(void *p, unsigned long arg)
{
	(void)p;
	(void)arg;

	kprintf("Balloon thread starting\n");

	lock_acquire(balloon_lock);							//needs both locks for balloon to proceed
	lock_acquire(balloon_lock2);							//one of each lock will be released by Dandelion and Marigold

	kprintf("Balloon freed and Prince Dandelion escapes!\n");
	kprintf("Balloon thread done\n");

	lock_release(balloon_lock);
	lock_release(balloon_lock2);
	lock_release(main_thread_lock);							//after balloon thread runs, main thread can run
}

int
airballoon(int nargs, char **args)
{
	init_rope_list();
	
	balloon_lock = lock_create("balloon_lock");
	balloon_lock2 = lock_create("balloon_lock2");
	lock_acquire(balloon_lock);							//acquire balloon locks, will be released after Dandelion and Marigold threads
	lock_acquire(balloon_lock2);

	main_thread_lock = lock_create("main_thread_lock");				//this lock is released by balloon so that main thread depending on it can run
	lock_acquire(main_thread_lock);

	int err = 0;

	(void)nargs;
	(void)args;

	err = thread_fork("Marigold Thread",
			  NULL, marigold, NULL, 0);
	if(err)
		goto panic;

	err = thread_fork("Dandelion Thread",
			  NULL, dandelion, NULL, 0);
		
	if(err)
		goto panic;

	err = thread_fork("Lord FlowerKiller Thread",
			  NULL, flowerkiller, NULL, 0);
	if(err)
		goto panic;

	err = thread_fork("Air Balloon",
			  NULL, balloon, NULL, 0);
	if(err)
		goto panic;

	goto done;

panic:
	panic("airballoon: thread_fork failed: %s)\n",
	      strerror(err));
	
done:
	lock_acquire(main_thread_lock);							//after balloon finishes, main thread can finish up
	kprintf("Main thread done\n");
	lock_release(main_thread_lock);

	lock_destroy(balloon_lock);							//destroy all locks that were created
	lock_destroy(balloon_lock2);
	lock_destroy(main_thread_lock);
	
	for(int n = 0 ; n < NROPES ; n++){
		lock_destroy(rope_locks[n]);
	}

	ropes_left = NROPES;								//reset ropes_left

	return 0;
}
